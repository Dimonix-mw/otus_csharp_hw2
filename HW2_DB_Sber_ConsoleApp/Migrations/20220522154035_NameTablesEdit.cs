﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HW2_DB_Sber_ConsoleApp.Migrations
{
    public partial class NameTablesEdit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Оperation_Account_AccountID",
                table: "Оperation");

            migrationBuilder.DropForeignKey(
                name: "FK_Account_Custumer_OwnerID",
                table: "Account");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Custumer",
                table: "Custumer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Account",
                table: "Account");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Оperation",
                table: "Оperation");

            migrationBuilder.RenameTable(
                name: "Custumer",
                newName: "Custumers");

            migrationBuilder.RenameTable(
                name: "Account",
                newName: "Accounts");

            migrationBuilder.RenameTable(
                name: "Оperation",
                newName: "Оperations");

            migrationBuilder.RenameIndex(
                name: "IX_Account_OwnerID",
                table: "Accounts",
                newName: "IX_Accounts_OwnerID");

            migrationBuilder.RenameIndex(
                name: "IX_Оperation_AccountID",
                table: "Оperations",
                newName: "IX_Оperations_AccountID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Custumers",
                table: "Custumers",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Accounts",
                table: "Accounts",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Оperations",
                table: "Оperations",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Оperations_Accounts_AccountID",
                table: "Оperations",
                column: "AccountID",
                principalTable: "Accounts",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_Custumers_OwnerID",
                table: "Accounts",
                column: "OwnerID",
                principalTable: "Custumers",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Оperations_Accounts_AccountID",
                table: "Оperations");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_Custumers_OwnerID",
                table: "Accounts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Custumers",
                table: "Custumers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Accounts",
                table: "Accounts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Оperations",
                table: "Оperations");

            migrationBuilder.RenameTable(
                name: "Custumers",
                newName: "Custumer");

            migrationBuilder.RenameTable(
                name: "Accounts",
                newName: "Account");

            migrationBuilder.RenameTable(
                name: "Оperations",
                newName: "Оperation");

            migrationBuilder.RenameIndex(
                name: "IX_Accounts_OwnerID",
                table: "Account",
                newName: "IX_Account_OwnerID");

            migrationBuilder.RenameIndex(
                name: "IX_Оperations_AccountID",
                table: "Оperation",
                newName: "IX_Оperation_AccountID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Custumer",
                table: "Custumer",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Account",
                table: "Account",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Оperation",
                table: "Оperation",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Оperation_Account_AccountID",
                table: "Оperation",
                column: "AccountID",
                principalTable: "Account",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Account_Custumer_OwnerID",
                table: "Account",
                column: "OwnerID",
                principalTable: "Custumer",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
