﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2_DB_Sber_ConsoleApp.Models
{
    public class Account
    {
        public int ID { get; set; }
        [Required]
        [MinLength(20)]
        [MaxLength(20)]
        public string Number { get; set; }
        [Required]
        public Custumer Owner { get; set; }
        public double Balance { get; set; }

       public override string ToString()
        {
            return $"ID = {ID}, Number = {Number} , OwnerID = {Owner.ID}, Balance = {Balance}";
        }
    }
}
