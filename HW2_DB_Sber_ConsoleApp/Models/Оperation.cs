﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2_DB_Sber_ConsoleApp.Models
{
    public class Оperation
    {
        public int ID { get; set; }
        public Account Account { get; set; }
        public double Sum { get; set; }
        [Required]
        public DateOnly Date { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return $"ID = {ID}, Account = {Account?.Number}, Sum = {Sum}, Date = {Date}, Description = {Description}";
        }
    }
}
