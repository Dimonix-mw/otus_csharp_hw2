﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HW2_DB_Sber_ConsoleApp.Models;
using Microsoft.EntityFrameworkCore;

namespace HW2_DB_Sber_ConsoleApp.Data
{
    public class DBBankContext : DbContext
    {
        public DBBankContext(DbContextOptions<DBBankContext> options)
            : base(options)
        {
            //Database.EnsureDeleted();   // удаляем бд со старой схемой
            //Database.EnsureCreated();   // создаем бд с новой схемой
        }

        public DbSet<Custumer> Custumers { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Оperation> Оperations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Custumer>().ToTable("Custumers");
            modelBuilder.Entity<Account>().ToTable("Accounts");
            modelBuilder.Entity<Оperation>().ToTable("Оperations");
        }

    }
}
